# Super App
Here you will see how a simple app design is created using basic tools on Sketch.

## Demo
### Sketch UI
![super_app_sketch_ui](.screenshots/super_app_sketch_ui.png)

### Exported
![super_app_exported](.screenshots/super_app_exported.png)
