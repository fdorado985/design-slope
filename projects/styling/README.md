# Styling
When we talk about `styling` is about that mind idea become true.
Whatever we think... rounded borders, colors, blur, transparency.
Think on your design and create it!

## Demo
### Request Permissions
![request_permission_sketch_ui](.screenshots/request_permissions_sketch_ui.png)

### Take Photo
![take_photo_sketch_ui](.screenshots/take_photo_sketch_ui.png)
